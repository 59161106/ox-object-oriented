
public class Player {
	private String name;
	private boolean x ;
	private int win = 0;
	private int draw = 0;
	private int lose = 0;
	
	public Player(String name, boolean x) {
		this.name = name;
		this.x = x;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	public boolean getX() {
		return x;
	}

	public int getWin() {
		return win;
	}

	public void setWin(int win) {
		this.win = win;
	}

	public int getDraw() {
		return draw;
	}

	public void setDraw(int draw) {
		this.draw = draw;
	}

	public int getLose() {
		return lose;
	}

	public void setLose(int lose) {
		this.lose = lose;
	}
	
	
	
}
