import java.util.Scanner;

public class Game {
	Scanner kb = new Scanner(System.in);
	private Board board;
	private char[][] sboard;
	private Player player1;
	private Player player2;
	boolean currentX;
	private int count = 0;
	
	
	public Game() {
//		player1 = new Player();
//		player2 = new Player();	
//		board = new Board(player1, player2);
	}
	
	
	public void playGame() {
		showWelcome();
		choosePlayer();
		while(true) {
			showBoard();
			showTurn();
			input();
			count++;
			board.switchPlayer();
			if (board.checksWinX() == true || board.checksWinO() == true || count > 8) {
				showBoard();
				showWin();
				showScore();
				if(playAgain() == false) {
					showBye();
					break;
				}
			}
			
		}
		
	}
	
	public void showWelcome() {
		System.out.println("---||| Welcome to OX game. |||---");
		System.out.println();
	}
	
	public void choosePlayer() {
		char choose;
		String name1,name2;
		System.out.println("You want to play X or O ?");
		System.out.print("Please enter your character : ");
		choose = kb.next().charAt(0);
		if(choose == 'X' || choose == 'x') {
			System.out.print("Input name player1 : ");
			name1 = kb.next();
			System.out.print("Input name player2 : ");
			name2 = kb.next();
			player1 = new Player(name1,true);
			player2 = new Player(name2,false);
		}else if(choose == 'O' || choose == 'o') {
			System.out.print("Input name player1 : ");
			name1 = kb.next();
			System.out.print("Input name player2 : ");
			name2 = kb.next();
			player1 = new Player(name1,false);
			player2 = new Player(name2,true);
		}else {
			System.out.println();
			System.out.println("Please choose again.");
			choosePlayer();
		}
		board = new Board(player1, player2);
		System.out.println();
	}
	
	public void showBoard() {
		char[][] sboard;
		sboard = board.getBoard();
		for(int i=0; i<4; i++) {
			for(int j=0; j<4; j++) {
				System.out.print(sboard[i][j] + " ");
			}System.out.println();
		}
	}
	
	public void showTurn() {
		
		currentX = board.currentPlayer();
		if(currentX == true) {
			System.out.println("X turn.");
		}else {
			System.out.println("O turn.");
		}
	}
	
	public void input() {
		int row;
		int col;
		sboard = board.getBoard();
		System.out.print("Please input row & col (R C) : ");
		row = kb.nextInt();
		col = kb.nextInt();
		if(board.checkBoard(row, col) == true) {
			if(currentX == true) {
				sboard[row][col] = 'X';
				board.checkBoard(row, col);
				
			}else{
				sboard[row][col] = 'O';
				board.checkBoard(row, col);
				
			}
		}else {
			System.out.println();
			System.out.println("Please check your position & input again.");
			System.out.println();
			input();
		}
		
	}
	
	public void showWin() {
		if(board.checksWinX() == true) {
			if(player1.getX() == true) {
				System.out.println("Player "+ player1.getName() + " WIN!!!");
				player1.setWin(player1.getWin()+1);
				player2.setLose(player2.getLose()+1);
			}else {
				System.out.println("Player "+ player2.getName() + " WIN!!!");
				player1.setLose(player1.getLose()+1);
				player2.setWin(player2.getWin()+1);
			}
		}else if(board.checksWinO() == true) {
			if(player1.getX() == false) {
				System.out.println("Player "+ player1.getName() + " WIN!!!");
				player1.setWin(player1.getWin()+1);
				player2.setLose(player2.getLose()+1);
			}else {
				System.out.println("Player "+ player2.getName() + " WIN!!!");
				player1.setLose(player1.getLose()+1);
				player2.setWin(player2.getWin()+1);
			}
		}else if(count > 8) {
			System.out.println("DRAW!!!");
			player1.setDraw(player1.getDraw()+1);
			player2.setDraw(player2.getDraw()+1);
		}
	}
	
	public void showScore() {
		System.out.println("********************** score **********************");
		System.out.println(player1.getName());
		System.out.println("Win : "+player1.getWin());
		System.out.println("Draw : "+player1.getDraw());
		System.out.println("Lose : "+player1.getLose());
		System.out.println(player2.getName());
		System.out.println("Win : "+player2.getWin());
		System.out.println("Draw : "+player2.getDraw());
		System.out.println("Lose : "+player2.getLose());
		System.out.println("***************************************************");
	}
	
	public boolean playAgain() {
		char yon;
		System.out.print("You want to play again?? (y/n) : ");
		yon = kb.next().charAt(0);
		if(yon == 'y') {
			board.setCurrent(player1.getX());
			board.resetBoard();
			count = 0;
			System.out.println("Player 1 turn");
			return true;
		}else if(yon == 'n'){
			return false;
		}else {
			return playAgain();
		}
	}
	
	public void showBye() {
		System.out.println("BYE BYE !!!!");
	}
	
}
