public class Board {
	private char[][] board = {{' ', '1', '2', '3'},{'1', '-', '-', '-'},{'2', '-', '-', '-'},{'3', '-', '-', '-'}};
	private Player player1;
	private Player player2;
	private boolean current;
	
	public Board(Player player1, Player player2) {
		this.player1 = player1;
		this.player2 = player2;
		this.current = player1.getX();
	}

	public char[][] getBoard() {
		return board;
	}
	
	public boolean currentPlayer() {
		return current;
		
	}
	
	public void switchPlayer() {
		if(current == true) {
			current = false;
		}else {
			current = true;
		}
	//	System.out.println("switch");
	}	

	public boolean checkBoard(int row, int col) {
		if((row > 0 && row < 4)&&(col > 0 && col < 4)&&(board[row][col] == '-')) {
			return true;
		}else {
			return false;
		}
		
	}
	
	public boolean checksWinX() {
		//	/\
		if((board[1][1] == 'X' && board[2][2] == 'X' && board[3][3] == 'X') || 
	    		(board[1][3] == 'X' && board[2][2] == 'X' && board[3][1] == 'X')) {
	    		 return true;
	    //	----
	    }else if((board[1][1] == 'X' && board[1][2] == 'X' && board[1][3] == 'X') || 
	    		(board[2][1] == 'X' && board[2][2] == 'X' && board[2][3] == 'X') ||
	    		(board[3][1] == 'X' && board[3][2] == 'X' && board[3][3] == 'X')) {
	    		 return true; 
	    //	|
	    }else if((board[1][1] == 'X' && board[2][1] == 'X' && board[3][1] == 'X') || 
	    		(board[1][2] == 'X' && board[2][2] == 'X' && board[3][2] == 'X') ||
	    		(board[1][3] == 'X' && board[2][3] == 'X' && board[3][3] == 'X')) {
	    		 return true; 
	    }else {
	    	return false;
	    }	
	
	}
	
	public boolean checksWinO() {
		//	/\
		if((board[1][1] == 'O' && board[2][2] == 'O' && board[3][3] == 'O') || 
	    		(board[1][3] == 'O' && board[2][2] == 'O' && board[3][1] == 'O')) {
	    		 return true;
	    //	----
	    }else if((board[1][1] == 'O' && board[1][2] == 'O' && board[1][3] == 'O') || 
	    		(board[2][1] == 'O' && board[2][2] == 'O' && board[2][3] == 'O') ||
	    		(board[3][1] == 'O' && board[3][2] == 'O' && board[3][3] == 'O')) {
	    		 return true; 
	    //	|
	    }else if((board[1][1] == 'O' && board[2][1] == 'O' && board[3][1] == 'O') || 
	    		(board[1][2] == 'O' && board[2][3] == 'O' && board[3][2] == 'O') ||
	    		(board[1][3] == 'O' && board[2][3] == 'O' && board[3][3] == 'O')) {
	    		 return true; 
	    }else {
	    	return false;
	    }	
	
	}
	
	public void resetBoard() {
		board[1][1] = '-';board[1][2] = '-';board[1][3] = '-';
		board[2][1] = '-';board[2][2] = '-';board[2][3] = '-';
		board[3][1] = '-';board[3][2] = '-';board[3][3] = '-';
	}

	public void setCurrent(boolean current) {
		this.current = current;
	}
	
	


	
	
}
